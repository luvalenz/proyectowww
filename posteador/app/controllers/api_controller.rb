require 'net/http'


class ApiController < ApplicationController
  def index
  end
  
  def search
    @method = params[:method]
    @path = params[:path]
    @hash_s = params[:hash]
    @token = params[:token]

    @hash = eval(@hash_s)

    @params_string = URI.escape(@hash.collect{|k,v| "#{k}=#{v}"}.join('&'))
    @params_string = '?' + @params_string.to_s 


    @url = @path + @params_string

    if @token && @token != ""
      @url += '&access_token=' + @token
    end

       
  end
  
  def form_prueba
  
  end
  
  def post_prueba
    @algo = params[:algo]
  end 
    
end
