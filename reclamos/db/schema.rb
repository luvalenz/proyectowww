# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121202182434) do

  create_table "causes", :force => true do |t|
    t.string   "name",                       :null => false
    t.integer  "popularity",  :default => 0
    t.text     "description",                :null => false
    t.integer  "state",       :default => 0
    t.integer  "company_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.integer  "lawyer_id"
  end

  create_table "chat_rooms", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "chats", :force => true do |t|
    t.integer  "chat_room_id"
    t.integer  "user_id"
    t.integer  "lawyer_id"
    t.integer  "company_id"
    t.string   "name"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.integer  "news"
  end

  create_table "companies", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "username"
    t.string   "password_digest"
    t.integer  "state"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "complaints", :force => true do |t|
    t.boolean  "frequent"
    t.string   "name"
    t.date     "problem_date"
    t.integer  "state",        :default => 0
    t.integer  "creator_id"
    t.integer  "cause_id"
    t.text     "description"
    t.integer  "views",        :default => 0
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.integer  "company_id"
  end

  add_index "complaints", ["creator_id"], :name => "index_complaints_on_creator_id"

  create_table "complaints_tags", :force => true do |t|
    t.integer "tag_id"
    t.integer "complaint_id"
  end

  create_table "complaints_users", :id => false, :force => true do |t|
    t.integer "supported_complaint_id"
    t.integer "supporter_id"
  end

  create_table "defense_requests", :force => true do |t|
    t.integer  "requested_cause_id"
    t.integer  "requesting_lawyer_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "documents", :force => true do |t|
    t.string   "name"
    t.string   "path"
    t.boolean  "photo"
    t.integer  "complaint_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "invitations", :force => true do |t|
    t.integer  "state",               :default => 0
    t.integer  "invited_id"
    t.integer  "inviting_id"
    t.integer  "cause_id"
    t.boolean  "visible_to_inviting", :default => true
    t.boolean  "visible_to_invited",  :default => true
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
  end

  create_table "lawyers", :force => true do |t|
    t.string   "username"
    t.string   "password_digest"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "curriculum_path"
    t.string   "email"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.boolean  "accepted",        :default => false
  end

  create_table "messages", :force => true do |t|
    t.string   "text"
    t.integer  "user_id"
    t.integer  "company_id"
    t.integer  "lawyer_id"
    t.integer  "chat_room_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "posts", :force => true do |t|
    t.string   "username"
    t.integer  "complaint_id"
    t.string   "text"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "recovery_codes", :force => true do |t|
    t.integer  "code"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "supports", :force => true do |t|
    t.boolean  "is_owner"
    t.integer  "user_id"
    t.integer  "cause_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "tags", :force => true do |t|
    t.string   "text"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "username"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.integer  "access_token"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.boolean  "admin"
  end

end
