class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :name
      t.string :path
      t.boolean :photo
      t.integer :complaint_id

      t.timestamps
    end
  end
end
