class ComplaintsTags < ActiveRecord::Migration
  def change
    create_table :complaints_tags do |t|
      t.integer :tag_id
      t.integer :complaint_id
    end
  end
end
