class CreateComplaints < ActiveRecord::Migration
  def change
    create_table :complaints do |t|
      t.boolean :frequent
      t.string :name
      t.date :problem_date
      t.integer :state, :default => 0
      t.integer :creator_id
      t.integer :cause_id
      t.text :description
      t.integer :views, :default => 0

      t.timestamps
    end
    add_index :complaints, :creator_id
  end
end
