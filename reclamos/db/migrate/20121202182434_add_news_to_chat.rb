class AddNewsToChat < ActiveRecord::Migration
  def change
    add_column :chats, :news, :integer
    Chat.all.each do |c|
      c.news = 0
      c.save
    end
  end
end
