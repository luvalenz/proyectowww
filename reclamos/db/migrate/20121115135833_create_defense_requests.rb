class CreateDefenseRequests < ActiveRecord::Migration
  def change
    create_table :defense_requests do |t|
      t.integer :requested_cause_id
      t.integer :requesting_lawyer_id

      t.timestamps
    end
  end
end
