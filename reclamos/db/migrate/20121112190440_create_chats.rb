class CreateChats < ActiveRecord::Migration
  def change
    create_table :chats do |t|
      t.integer :chat_room_id
      t.integer :user_id
      t.integer :lawyer_id
      t.integer :company_id
      t.string  :name

      t.timestamps
    end
  end
end
