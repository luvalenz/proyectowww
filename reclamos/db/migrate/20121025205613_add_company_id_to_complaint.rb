class AddCompanyIdToComplaint < ActiveRecord::Migration
  def change
    add_column :complaints, :company_id, :integer
  end
end
