class AddReadToChat < ActiveRecord::Migration
  def change
    add_column :chats, :read, :datetime
    Chat.all.each do |c|
      c.read = 0
      c.save
    end
  end
end
