class RemoveReadToChat < ActiveRecord::Migration
  def change
    remove_column :chats, :read
  end
end
