class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.integer :state, :default => 0
      t.integer :invited_id
      t.integer :inviting_id
      t.integer :cause_id
      t.boolean :visible_to_inviting, :default => true
      t.boolean :visible_to_invited, :default => true

      t.timestamps
    end
  end
end
