class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :email
      t.string :username
      t.string :password
      t.integer :state

      t.timestamps
    end
  end
end
