class CreateCauses < ActiveRecord::Migration
  def change
    create_table :causes do |t|
      t.string :name, :null => false
      t.integer :popularity, :default => 0
      t.text :description, :null => false
      t.integer :state, :default => 0
      t.integer :company_id

      t.timestamps
    end
  end
end
