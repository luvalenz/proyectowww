class ChangeDataTypeForUserId < ActiveRecord::Migration
  def up
    change_table :posts do |t|
      t.change :user_id, :string
    end
  end
end
