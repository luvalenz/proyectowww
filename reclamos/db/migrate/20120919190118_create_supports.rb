class CreateSupports < ActiveRecord::Migration
  def change
    create_table :supports do |t|
      t.boolean :is_owner
      t.integer :user_id
      t.integer :cause_id

      t.timestamps
    end
  end
end
