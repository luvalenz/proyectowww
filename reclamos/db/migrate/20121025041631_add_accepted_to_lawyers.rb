class AddAcceptedToLawyers < ActiveRecord::Migration
  def change
      add_column :lawyers, :accepted, :boolean, :default => false
      Lawyer.all.each do |u|
        u.accepted = false
        u.save
      end
  end
end
