class CreateComplaintUserJoinTable < ActiveRecord::Migration
  def change
    create_table :complaints_users, :id => false do |t|
      t.integer :supported_complaint_id
      t.integer :supporter_id
    end
  end
end
