class AddLawyerIdToCauses < ActiveRecord::Migration
  def change
    add_column :causes, :lawyer_id, :integer
  end
end
