class CreateLawyers < ActiveRecord::Migration
  def change
    create_table :lawyers do |t|
      t.string :username
      t.string :password
      t.string :first_name
      t.string :last_name
      t.string :curriculum_path
      t.string :email

      t.timestamps
    end
  end
end
