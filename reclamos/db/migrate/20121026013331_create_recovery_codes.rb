class CreateRecoveryCodes < ActiveRecord::Migration
  def change
    create_table :recovery_codes do |t|
      t.integer :code
      t.integer :user_id

      t.timestamps
    end
  end
end
