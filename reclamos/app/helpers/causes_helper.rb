module CausesHelper

  def list_complaints(complaints)
    complaints.each{|x| yield(x)}
  end
  
  def list_users(users)
    users.each{|x| yield(x)}
  end
 
 

end
