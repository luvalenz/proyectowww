class ApplicationController < ActionController::Base
  protect_from_forgery
  
  helper_method :current_user
  helper_method :current_account
  helper_method :current_is_user
  helper_method :current_is_admin
  helper_method :current_is_lawyer
  helper_method :current_is_company
  helper_method :invitations
  helper_method :get_account_by_username
  helper_method :get_msg_count
  
  protected
  def invitations
    debugger
    if (@_current_user != nil)
      @_current_user.received_invitations.where(:state => 0).length
    end
  end
  
  
  protected
  def current_user
    debugger
    
    if(session[:account]=='user')
      @_current_user ||= User.find(session[:id])
    elsif(session[:account]=='lawyer')
      @_current_user ||= Lawyer.find(session[:id])
    elsif(session[:account]=='company')
      @_current_user ||= Company.find(session[:id])
    end
  end
  
  protected
  def current_account
    debugger
   if(session[:account]=='user')
      @_current_account ||= 'user'
    elsif(session[:account]=='lawyer')
      @_current_account ||= 'lawyer'
    elsif(session[:account]=='company')
      @_current_account ||= 'company'
    end
  end
  
  protected
  def current_is_admin
    debugger
    if(session[:account]=='user')
      if User.find(session[:id]).admin == true 
        return true
      end
    else
      return false
    end
  end
  
  protected
  def current_is_user
    debugger
    if(session[:account]=='user')
      return true
    else
      return false
    end
  end
  
  protected
  def current_is_lawyer
    debugger
    if(session[:account]=='lawyer')
      return true
    else
      return false
    end
  end
  
  protected
  def current_is_company
    debugger
    if(session[:account]=='company')
      return true
    else
      return false
    end
  end
  
  protected
  def get_account_by_username(username)
    debugger
    @user = User.find_by_username(username)
    @lawyer = Lawyer.find_by_username(username)
    @company = Company.find_by_username(username)
    if !@user.nil? 
      return @user
    elsif !@lawyer.nil? 
      return @lawyer
    else 
      return @company
    end
  end
  
  protected
  def get_msg_count
    @count = 0
    if current_is_user then
      @chats = Chat.find_all_by_user_id(current_user.id)
    end
    if current_is_lawyer then
      @chats = Chat.find_all_by_lawyer_id(current_user.id)
    end
    if current_is_company then
      @chats = Chat.find_all_by_company_id(current_user.id)
    end
    unless @chats.nil? then
      @chats.each{ |c|
        @count += c.news
      }
    end
    return @count
  end
  
end
