class UsersController < ApplicationController

  # mostramos los subscritos
  #def show_users_of_complaint
  #  @users = User.find_all_by_complaint_id(params[:id])
  #  render :index
  #end

  def users_ajax
    if params[:term]
      like= "%".concat(params[:term].concat("%"))
      users = User.where("username like ?", like)
    else
      users = User.all
    end
    list = users.map {|u| Hash[ id: u.id, label: u.username, name: u.username]}
    render json: list
  end


  # GET /users
  # GET /users.json
  def index
    @users = User.all
    
      if current_is_admin
        respond_to do |format|
          format.html 
          format.json { render json: @users }
        end
      else
        render :inline => 'Debe ser admin para ver esto.', :layout => true
      end  

  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
    
    #UserMailer.welcome_email(@user).deliver

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    if current_is_admin 

      @user = User.new
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @user }
      end
    else
      redirect_to index_path
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
    if current_is_admin or current_user == @user
      @user = User.find(params[:id])
    else
      redirect_to index_path
    end
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save    
        
#        UserMailer.welcome_email(@user).deliver
           
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end
  
  def my_invitations
    @received = current_user.received_invitations
    @sent = current_user.sent_invitations
        
    @received_is_empty = true
    if !@received.empty?
      @received.each do |i|
        if i.visible_to_invited
          @received_is_empty = false
        end
      end
    else
      @received_is_empty = true
    end
    
    @sent_is_empty = true
    if !@sent.empty?
      @sent.each do |i|
        if i.visible_to_inviting
          @sent_is_empty = false
        end
      end
    else
      @sent_is_empty = true
    end  
    
    
    
  end
  
  
  # GET /users/1/change_password
  def change_password
    @user = User.find(params[:id])
  end
  
  # POST /users/1/update_password
  def update_password
    @user = User.find(params[:id])
    
    @new_password = params[:password]
    @code = RecoveryCode.find_by_code(params[:code_number])
       
    if @user.recovery_codes.include?(@code)
      @contains = true        
      if @user.update_attributes(:password => @new_password)
        @code.delete
        @modificado = true
      end
    end
  end
  
  
  def api
    @user = User.find(current_user.id)
    @token = @user.access_token
    
  end
  
  def generate_token
    user = User.find(current_user.id)
    token = Random.rand(1000000..2000000)
    user.update_attribute(:access_token, token)
    
    redirect_to api_user_path(current_user)
    
  end
   
end
