class MessagesController < ApplicationController

  def index
    if current_is_user then
      @chats = Chat.find_all_by_user_id(current_user.id)
    elsif current_is_lawyer then
      @chats = Chat.find_all_by_lawyer_id(current_user.id)
    else
      @chats = Chat.find_all_by_company_id(current_user.id)
    end
  end

  def new
  end

  def create
    @participantes = params[:participantes]
    @texto = params[:mensaje]
    @asunto = params[:asunto]
    
    @chatroom = ChatRoom.new
    @chatroom.name = @asunto

    @msg = Message.new
    @msg.text = @texto
    @msg.chat_room = @chatroom
    
    @chat = Chat.new
    @chat.news = 0
    if current_is_user then
      @msg.user = current_user
      @chat.user = current_user
    elsif current_is_lawyer then
      @msg.lawyer = current_user
      @chat.user = current_user
    else
      @msg.company = current_user
      @chat.user = current_user
    end
    @chatroom.chats << @chat
    @chat.save
    
    @msg.save
    
    @participantes.each {|p|
      # Creamos chat
      @chat = Chat.new
      @account = get_account_by_username(p)
      if @account.class.name == "User" then
        @chat.user = @account
      elsif @account.class.name == "Lawyer" then
        @chat.lawyer = @account
      else
        @chat.company = @account
      end
      @chat.news = 1
      @chatroom.chats << @chat
      @chat.save
    }
    
    @chatroom.messages << @msg
    @chatroom.save
    
    redirect_to @msg
    
  end
  
  def show
    
    @chatroom = ChatRoom.find_by_id(params[:id])
    
    if @chatroom.nil? then
      return redirect_to :action => "index"
    end
    
    @messages = Message.find_all_by_chat_room_id(@chatroom.id,:order => 'created_at ASC')
    
    @chats = Chat.find_all_by_chat_room_id(params[:id])
    
    estoy_incluido = false;
    @chats.each{ |c|
      if(c.lawyer == current_user)
        c.news = 0
        c.save
        estoy_incluido = true;
      elsif(c.user == current_user)
        c.news = 0
        c.save
        estoy_incluido = true;
      elsif(c.user == current_user)
        c.news = 0
        c.save
        estoy_incluido = true;
      end
    }
    
    if(estoy_incluido == false)
      return redirect_to :action => "index"
    end
    
  end
  
  def send_message
    @text = params[:message]
    @chatroom = ChatRoom.find(params[:chatroom])
    
    @msg = Message.new
    @msg.add_current(current_user)
    @msg.chat_room = @chatroom
    @msg.text = @text
    @msg.save
    
    @chatroom.messages << @msg
    @chatroom.save
    
    @chatroom.chats.each { |c|
      c.update_attributes(:news => c.news+1)
    }
    
    redirect_to :action => "show", :id => @chatroom.id
  end

end
