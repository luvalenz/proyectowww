class CausesController < ApplicationController
  # GET /causes
  # GET /causes.json
  def index
    @causes = Cause.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @causes }
    end
  end

  # GET /causes/1
  # GET /causes/1.json
  def show
    @cause = Cause.find(params[:id])
    @complaints = @cause.complaints
    @users = @cause.users
    @lawyer = @cause.lawyer
    @company = @cause.company
    
    @exists_request = nil
    
    
    if (current_is_lawyer)    
      @exists_request = DefenseRequest.where(:requesting_lawyer_id => current_user.id, :requested_cause_id => @cause.id)
    end
    
    
    if (@exists_request == [])
      @exists_request = nil
    end
    
    
    if (current_is_lawyer)
      @defend = true
    else
      @defend = false
    end
    
    
    @supports = false
    
    if @cause.users.include?(current_user)
      @supports = true                            
    end
    
    

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @cause }
    end
  end

  # GET /causes/new
  # GET /causes/new.json
  def new
    @cause = Cause.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @cause }
    end
  end
  
  def new_with_complaint 
    @complaint = Complaint.find_by_id(params[:complaint_id])
    @company = Company.find(@complaint.company.id) 
     
    @cause = Cause.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @cause }
    end
  end

  # GET /causes/1/edit
  def edit
    @cause = Cause.find(params[:id])
  end

  # POST /causes
  # POST /causes.json
  def create
    @cause = Cause.new(params[:cause])
    
    respond_to do |format|
      if @cause.save
        format.html { redirect_to @cause, notice: 'Cause was successfully created.' }
        format.json { render json: @cause, status: :created, location: @cause }
      else
        format.html { render action: "new" }
        format.json { render json: @cause.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def create_with_complaint
    @access_token = params[:access_token]
    @complaint_id = params[:complaint_id]
    @complaint = Complaint.find_by_id(@complaint_id)
     
    @cause_hash = {}
     
    @cause_hash[:company_id] = params[:company_id]
    @cause_hash[:description] = params[:description]
    @cause_hash[:name] = params[:name]

     
    @cause = Cause.new(@cause_hash)  
    
    
    if @access_token
      @creator = User.find_by_access_token(@access_token)
    else
      @creator = current_user
    end
    
    if @creator
      save = @cause.save
    end
     
    if save
      @cause.complaints << @complaint     
      @cause.users << @creator
      @support = Support.where(:user_id => @creator.id, :cause_id => @cause.id)[0]
           
      @support.is_owner = true
      
      @support.save
      
      
      respond_to do |format|
        format.html { redirect_to @complaint, notice: "La causa para el reclamo fue creada con exito"}
        format.json { render json: @cause }
      end

    end
      
  end

  # PUT /causes/1
  # PUT /causes/1.json
  def update
    @cause = Cause.find(params[:id])

    respond_to do |format|
      if @cause.update_attributes(params[:cause])
        format.html { redirect_to @cause, notice: 'Cause was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @cause.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /causes/1
  # DELETE /causes/1.json
  def destroy
    @cause = Cause.find(params[:id])
    @cause.destroy

    respond_to do |format|
      format.html { redirect_to causes_url }
      format.json { head :no_content }
    end
  end
  
  #GET
  def support
    @cause = Cause.find(params[:id])
    
    @supports = false
    
    if @cause.users.include?(current_user)
      @supports = true
    end    
    
    if current_user != nil && !@supports
      @cause.users << current_user
      redirect_to @cause
    end
  end
  
  #GET
  def unsupport
    @cause = Cause.find(params[:id])
    
    @supports = false   
    if @cause.users.include?(current_user)
      @supports = true
    end
    
    if current_user != nil && @supports
      @cause.users.delete(current_user)
      redirect_to @cause
    end      
  end
  
  #POST
  def defend
    @cause = Cause.find(params[:id])
    if current_is_lawyer
      @existing_request = @cause.requesting_lawyers.where(:id => current_user.id)
      if @existing_request != nil && !@existing_request.empty?
        redirect_to @cause, notice: 'Usted ya mando una solicitud de defensa para esta causa'
      else
        @dr = DefenseRequest.new(:requesting_lawyer_id => current_user.id, :requested_cause_id => @cause.id)
        if @dr.save
          redirect_to @cause, notice: 'Se le ha enviado al creador de la causa una solicitud de defensa'
        end
      end
    end
  end
  
  #POST
  def solve
    @cause = Cause.find(params[:id])
    @cause.update_attributes(:state => 2)
    redirect_to @cause
  end
  
end
