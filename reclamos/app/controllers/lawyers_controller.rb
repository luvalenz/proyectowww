class LawyersController < ApplicationController
  # GET /lawyers
  # GET /lawyers.json
  def index
    @lawyers = Lawyer.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @lawyers }
    end
  end

  # GET /lawyers/1
  # GET /lawyers/1.json
  def show
    @lawyer = Lawyer.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @lawyer }
    end
  end

  # GET /lawyers/new
  # GET /lawyers/new.json
  def new
    if current_is_admin
      @lawyer = Lawyer.new
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @lawyer }
      end
    else
      redirect_to index_path
    end
  end

  # GET /lawyers/1/edit
  def edit
    if current_is_admin or (current_account=='lawyer' and current_user.id==params[:id])
      @lawyer = Lawyer.find(params[:id])
    else
      redirect_to index_path
    end
  end

  # POST /lawyers
  # POST /lawyers.json
  def create
    @lawyer = Lawyer.new(params[:lawyer])

    respond_to do |format|
      if @lawyer.save
        format.html { redirect_to @lawyer, notice: 'Lawyer was successfully created.' }
        format.json { render json: @lawyer, status: :created, location: @lawyer }
      else
        format.html { render action: "new" }
        format.json { render json: @lawyer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /lawyers/1
  # PUT /lawyers/1.json
  def update
    @lawyer = Lawyer.find(params[:id])

    respond_to do |format|
      if @lawyer.update_attributes(params[:lawyer])
        format.html { redirect_to @lawyer, notice: 'Lawyer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @lawyer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lawyers/1
  # DELETE /lawyers/1.json
  def destroy
    @lawyer = Lawyer.find(params[:id])
    @lawyer.destroy

    respond_to do |format|
      format.html { redirect_to lawyers_url }
      format.json { head :no_content }
    end
  end
  
  def accept
    if params[:lawyer]
      if(params[:accion]=='aceptar')
        if @lawyer = Lawyer.find(params[:lawyer])
          @lawyer.update_column(:accepted,true)
          LawyerMailer.acception_email(@lawyer).deliver
        end
      else
        if @lawyer = Lawyer.find(params[:lawyer])
          @lawyer.destroy
        end
      end
    end
    @lawyers = Lawyer.find(:all, :conditions => {:accepted => false})
  end
  
  def defend_cause
    @lawyer = Lawyer.find(params[:id])
    @cause = Cause.find(params[:cause_id])
    
    @cause.lawyer = @lawyer
    
    if @cause.save
      @old_request_arr = DefenseRequest.where(:requested_cause_id => @cause.id, :requesting_lawyer_id => @lawyer.id)
      
      @old_request = @old_request_arr[0]
      
      @old_request.destroy
    end
    
    redirect_to @cause, notice: 'Ahora la causa tiene defensor'
  end
  
  def stop_defending_cause
    @lawyer = Lawyer.find(params[:id])
    @cause = Cause.find(params[:cause_id])
    
    @cause.lawyer = nil
    
    if @cause.save
      redirect_to @cause, notice: 'Has despedido al abogado'
    end
  end
  
end
