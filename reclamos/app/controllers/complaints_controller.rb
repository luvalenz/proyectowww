class ComplaintsController < ApplicationController
  # GET /complaints
  # GET /complaints.json
  
  def index  
    searched_tag = params[:tag]
    my_complaints = params[:my_complaints]
    not_existing_tag = false
    
    
    
    if (searched_tag != nil)
      tag = Tag.find_by_text(searched_tag)
      if tag != nil       
        @complaints = tag.complaints
      else
        not_existing_tag = true
      end
    elsif my_complaints
      @complaints = current_user.created_complaints.order(:created_at => :asc)
    else
      @complaints = Complaint.all
    end
    
    
    
    
    if not_existing_tag
      redirect_to no_tag_complaints_url
    else
      @complaints.sort! { |a,b| b.likes <=> a.likes }  
      respond_to do |format|
        format.html # index.html.erb
        format.json do
          @access_token = params[:access_token]
          if @access_token
            @complaints = User.find_by_access_token(@access_token).created_complaints
          end
          render json: @complaints 
        end
      end
    end
    
  end

  # GET /complaints/1
  # GET /complaints/1.json
  def show
    @complaint = Complaint.find(params[:id])
    @cause =  Cause.find_by_id(@complaint.cause_id)
    @tags = @complaint.tags
    
    @complaint.update_attributes(:views => (1 + @complaint.views))
    
    if @cause.nil? == false 
      @cause_name =  @cause.name
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @complaint }
    end
  end

  # GET /complaints/new
  # GET /complaints/new.json
  def new
    if current_account == 'user'
      @complaint = Complaint.new

      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @complaint }
      end
    else
      redirect_to index_path
    end
  end

  # GET /complaints/1/edit
  def edit
    @complaint = Complaint.find(params[:id])
  end

  # POST /complaints
  # POST /complaints.json
  def create
    @token = params[:access_token]
    @complaint = Complaint.new(params[:complaint])
        

    if @token
      p = {}
      p[:frequent] = params[:frequent]
      p[:name] = params[:name]
      p[:problem_date] = params[:description]
      p[:company_name] = params[:company_name]
      @complaint = Complaint.new(p)
      @complaint.creator = User.find_by_access_token(@token)
    elsif current_user
      @complaint = Complaint.new(params[:complaint])
      @complaint.creator = current_user;
    end
    
       
    if @complaint.creator
      save = @complaint.save
    end
       
    if current_user && !@token
      files = params['files']
    end
    
    if files and save
      files.each_key{ |key| 
        document = Document.uploadFile(key,files[key])
        document.complaint = @complaint
        document.save
        @complaint.documents << document
      }
    end
    
    respond_to do |format|
      if save && !@token
        format.html { redirect_to assign_cause_complaints_url(:complaint_id => @complaint.id), notice: 'El reclamo fue creado con exito. Ahora debes asignarle una causa' }
        format.json { render json: @complaint, status: :created, location: @complaint }
     elsif save && @token
      format.html { redirect_to @complaint }
      format.json { render json: @complaint }
        
     else
        format.html { render action: "new", notice: 'Debes estar logueado para crear un reclamo' }
        format.json { render json: @complaint.errors, status: :unprocessable_entity }

     end
    end
  end

  # PUT /complaints/1
  # PUT /complaints/1.json
  def update
    @complaint = Complaint.find(params[:id])
    
    save = @complaint.update_attributes(params[:complaint])
    
    files = params['files']
    
    if files and save
      files.each_key{ |key| 
        document = Document.uploadFile(key,files[key])
        document.complaint = @complaint
        document.save
        @complaint.documents << document
      }
    end
    
    respond_to do |format|
      if save
        format.html { redirect_to complaints_url, notice: 'Complaint was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @complaint.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /complaints/1
  # DELETE /complaints/1.json
  def destroy
    @complaint = Complaint.find(params[:id])
    @complaint.destroy

    respond_to do |format|
      format.html { redirect_to complaints_url }
      format.json { head :no_content }
    end
  end
  
  def assign_cause
    @complaint = Complaint.find(params[:complaint_id])    
  end
  
  def select_existing_cause
    @complaint = Complaint.find(params[:complaint_id]) 
    
    @valid_causes = @complaint.company.causes
    if (@valid_causes == [])
      @valid_causes = nil
    end
    
  end
  
  #PUT /complaint/1/join_to_existing_cause  
  def join_to_existing_cause
    @complaint = Complaint.find(params[:complaint_id])
    @cause = Cause.find(params[:cause][:id])  
    @complaint.cause_id = @cause.id
    @complaint.save      
  end
  
  #GET 
  def add_tag
    @complaint = Complaint.find(params[:complaint_id])
  end
  
  
  #POST
  def tag_added
    @complaint = Complaint.find(params[:complaint_id])
    @tag_text = params[:new_tag]
    
    @length = @complaint.tags.length
    
    @existing_tag = Tag.find_by_text(@tag_text)
    
    @tag
       
    if @existing_tag
      @tag = @existing_tag
      if !@complaint.tags.include?(@existing_tag)
        @complaint.tags << @existing_tag
        @existing_tag = false
      else
        @existing_tag = true
      end
    else          
      @new_tag  = Tag.new(:text => @tag_text)
      @tag = @new_tag
    
      
      if @new_tag.save
        @complaint.tags << @new_tag
        @complaint.save
      end
    end
      
    respond_to do |format|
      format.html { redirect_to @complaint }
      format.js { }
    end
  end
  
  #GET  
  def remove_tag
    @complaint = Complaint.find(params[:complaint_id])
    @tag = Tag.find(params[:tag_id])
    @complaint.tags.delete(@tag)
    
    if @tag.complaints == [] || @tag.complaints == nil
      @tag.delete
    end
    
    redirect_to @complaint
   
  end
  
  def no_tag
  end
  
  
  #POST
  def post_added
    @complaint = Complaint.find(params[:complaint_id])
    if !params[:new_post].blank? 
      @post = Post.new
      @post.text = params[:new_post]
      @post.username = current_user.username
      @post.complaint = @complaint
      @post.save
    end
    respond_to do |format|
      format.html { redirect_to @complaint }
      format.js { }
    end
  end
  
  #POST
  def post_removed
    post = Post.find(params[:delete_post])
    @complaint = post.complaint
    post.destroy()
    respond_to do |format|
      format.html { redirect_to @complaint }
      format.js { }
    end
  end
  
end
