class InvitationsController < ApplicationController
  # GET /invitations
  # GET /invitations.json
  def index
    @invitations = Invitation.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @invitations }
    end
  end

  # GET /invitations/1
  # GET /invitations/1.json
  def show
    @invitation = Invitation.find(params[:id])
    @inviting = User.find_by_id(@invitation.inviting_id)
    @invited = User.find_by_id(@invitation.invited_id)
    @cause = Cause.find_by_id(@invitation.cause_id)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @invitation }
    end
  end

  # GET /invitations/new
  # GET /invitations/new.json
  def new
    @invitation = Invitation.new
    
    @users = User.all
    @users_except_current = User.all.delete_if { |x| x.id == session[:id] }

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @invitation }
    end
  end

  # GET /invitations/1/edit
  def edit
    @invitation = Invitation.find(params[:id])
  end

  # POST /invitations
  # POST /invitations.json
  def create
    @params = params[:invited]
  
    @invitation = Invitation.new(params[:invitation])
    @invitation.inviting_id = current_user.id
    @invitation.save
    
    @inviting_user = @invitation.inviting
    @invited_user = @invitation.invited
    @cause = @invitation.cause 
    
    
    UserMailer.send_invitation(@inviting_user, @invited_user, @cause).deliver
    
    respond_to do |format|
      if @invitation.save
        format.html { redirect_to my_invitations_user_path(current_user), notice: 'La invitacion fue enviada' }
        format.json { render json: @invitation, status: :created, location: @invitation }
      else
        format.html { render action: "new" }
        format.json { render json: @invitation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /invitations/1
  # PUT /invitations/1.json
  def update
    @invitation = Invitation.find(params[:id])

    respond_to do |format|
      if @invitation.update_attributes(params[:invitation])
        format.html { redirect_to @invitation, notice: 'Invitation was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @invitation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /invitations/1
  # DELETE /invitations/1.json
  def destroy
    @invitation = Invitation.find(params[:id])
    @invitation.destroy

    respond_to do |format|
      format.html { redirect_to invitations_url }
      format.json { head :no_content }
    end
  end
  
  def accept
    # Aceptamos invitacion
    @invitation = Invitation.find(params[:id])
    
    
    
    
    @invitation.state = 1
    if @invitation.save
      # Asignamos usuario a la causa
      @cause = @invitation.cause
      @inviting_user = @invitation.inviting
      @invited_user = @invitation.invited

      
      UserMailer.accepted_invitation(@inviting_user, @invited_user, @cause).deliver 
      
      unless @cause.users.include?(@invitation.invited )
         @cause.users << @invitation.invited 
      end      
      redirect_to @cause, notice: 'Ahora apoyas esta causa'
      
    end
  end
  
  def reject
    @invitation = Invitation.find(params[:id])
    @invitation.state = -1
    if @invitation.save
      @inviting_user = @invitation.inviting
      @invited_user = @invitation.invited      
      UserMailer.accepted_invitation(@inviting_user, @invited_user, @cause).deliver 
    
      redirect_to my_invitations_user_path(current_user)
    end
      
  end
  
  def hide_sent
    @invitation = Invitation.find(params[:id])
    
    if (@invitation.visible_to_invited)
      @invitation.visible_to_inviting = false
      @invitation.save
    else
      @invitation.destroy
    end
    redirect_to my_invitations_user_path(current_user)
  end
  
  def hide_received
    @invitation = Invitation.find(params[:id])
    
    if (@invitation.visible_to_inviting)
      @invitation.visible_to_invited = false
      @invitation.save
    else
      @invitation.destroy
    end
    redirect_to my_invitations_user_path(current_user)
  end
  
end
