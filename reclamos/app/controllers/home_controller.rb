class HomeController < ApplicationController
  def index
    @user = User.find(session[:id]) if session[:account]=='user'
    @user = Lawyer.find(session[:id]) if session[:account]=='lawyer'
  end

  def login
    #if(params[:account]=='user')
      if u = User.find_by_username(params[:username]).try(:authenticate,params[:password])
        session[:id] = u.id
        session[:account] = 'user'
        flash[:notice] = "Autentificado como usuario =)"
    #  end
    #elsif(params[:account]=='lawyer')
      elsif u = Lawyer.find_by_username(params[:username]).try(:authenticate,params[:password]) and u.accepted
        session[:id] = u.id
        session[:account] = 'lawyer'
        flash[:notice] = "Autentificado como Abogado =)"
      #end
    #elsif(params[:account]=='company')
      elsif u = Company.find_by_username(params[:username]).try(:authenticate,params[:password]) and u.state == 2 then
        session[:id] = u.id
        session[:account] = 'company'
        flash[:notice] = "Autentificado como Empresa"  
      end
    #end
    redirect_to :action => :index
  end

  def logout
    reset_session
    flash[:notice] = "Gracias por visitarnos"
    redirect_to :action => :index
  end
  
  def signup
    if(request.post?) then # POST
      @selection = params[:account_type] # valor usado para el select
      if(params[:account_type]=='user')
        @user = User.new(params[:user])
        @lawyer = Lawyer.new
        @company = Company.new
        if @user.save
          token = Random.rand(2000..100000)
          @user.access_token = token
          @user.save
          UserMailer.welcome_email(@user).deliver
          flash[:notice] = 'Cuenta creada con exito. Puede iniciar sesion.'
        end
      elsif(params[:account_type]=='lawyer')
        Lawyer.process_file(params[:lawyer])
        @lawyer = Lawyer.new(params[:lawyer])
        @user = User.new
        @company = Company.new
        if @lawyer.save
          LawyerMailer.welcome_email(@lawyer).deliver
          flash[:notice] = 'Cuenta creada con exito. Se le notificara por mail el resultado de su postulacion.'
        end
      else
        @company = Company.find_by_name(params[:company_name])
        if @company.nil? then
          @company = Company.new
          @company.name = params[:company_name]
        end
        @company.email = params[:company][:email]
        @company.username = params[:company][:username]
        @company.password = params[:company][:password]
        @company.state = 1
        if @company.save
          CompanyMailer.welcome_email(@company).deliver
          flash[:notice] = 'Cuenta creada con exito. Se le notificara por mail el resultado de su postulacion.'
        end
        @selection = 'company' 
        @user = User.new
        @lawyer = Lawyer.new
      end
    else # GET
      @user = User.new
      @company = Company.new
      @lawyer = Lawyer.new
      @selection = 'user'
    end
  end
  
  def admin
  end
  
  def recover_password
        
  end
  
  def send_password
    @username = params[:username]   
    @user = User.find_by_username(@username)
    
    if(@user)    
      @code = rand(0..100000000).to_s     
      @rc = RecoveryCode.new(:code => @code)
      @rc.user_id = @user.id
      @rc.save

      UserMailer.send_password(@user, @code).deliver
    end
          
  end
  
  def set_user_to_change_password    
  end
  
  def go_to_user_change_password
    @user = User.find_by_username(params[:username])
    
    redirect_to change_password_user_path(:id => @user.id)
  end
  
  def change_form
    respond_to do |format|
      format.js
    end
  end
  
end
