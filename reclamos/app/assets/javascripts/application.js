// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require_tree .

//--------------------------------------
// REGISTRO
//--------------------------------------

function swapNodes(a, b) {
    var aparent= a.parentNode;
    var asibling= a.nextSibling===b? a : a.nextSibling;
    b.parentNode.insertBefore(a, b);
    aparent.insertBefore(b, asibling);
}

var lastSelected;

function showDivs(selectValue){

  var UserNode = $("#signup_user");
  var LawyerNode = $("#signup_lawyer");
  var CompanyNode = $("#signup_company");
  
  if(lastSelected==null)
  {
    lastSelected = UserNode;
  }
  switch(selectValue)
  {
    case "user":
      /*UserNode.style.visibility ='visible';
      LawyerNode.style.visibility ='hidden';
      CompanyNode.style.visibility ='hidden';*/
      UserNode.show("slow");
      CompanyNode.hide("slow");
      LawyerNode.hide("slow");
      swapNodes(lastSelected[0],UserNode[0]);
      lastSelected = UserNode;
      break;
    case "lawyer":
      LawyerNode.show("slow");
      UserNode.hide("slow");
      CompanyNode.hide("slow");
      swapNodes(lastSelected[0],LawyerNode[0]);
      lastSelected = LawyerNode;
      break;
    case "company":
      CompanyNode.show("slow");
      UserNode.hide("slow");
      LawyerNode.hide("slow");
      swapNodes(lastSelected[0],CompanyNode[0]);
      lastSelected = CompanyNode;
      break;
  }
}

function registro_init(){
  // Revisamos si esta el div user_type solo en home/register
   if($("#user_type").length > 0){
      $("#signup_company").hide();
      $("#signup_user").hide();
      $("#signup_lawyer").hide();
      
      showDivs($("#user_type").val());
    
      $("#user_type").change(function(){
        showDivs($("#user_type").val());
      });
      
      $("#company_id").change(function(){
        if($("#company_id").val()==''){
          $("#signup_company_name").html('<input type="text" name="company_name"/>');
        } else {
          $("#signup_company_name").html('');
        }
      });
      
      $("#new_user").validate({
        rules: {
          'user[email]' : {required: true},
          'user[username]' : {required: true},
          'user[password]' : {required: true}
        },
        messages: {
          'user[email]' : {required: "Campo requerido"},
          'user[username]' : {required: "Campo requerido"},
          'user[password]' : {required: "Campo requerido"}
        }
      });
      
      
    }
}
//--------------------------------------
// COMPLAINT
//--------------------------------------
function complaint_init(){
    // Revisamos si esta el select con id complaint_frequent solo en complaint/new 
    if($('select#complaint_frequent').length > 0){
      if($('select#complaint_frequent').val()=='false'){
        $('#problem_date').show();
      } else {
        $('#problem_date').hide();
      }
      $('#complaint_frequent').change(function(){
        if($('#complaint_frequent').val()=='true'){
          $('#problem_date').hide();
        } else {
          $('#problem_date').show();
        }
      });
    }
    $('#new-tag-div').hide();
    $('#new-tag-button').click(function(){
      if($('#new-tag-button').val()=='Nuevo tag'){
        $('#new-tag-div').show();
        $('#new-tag-button').val('Cancelar');
        $('#new-tag').removeClass('error');
        $('label.error').remove();
      }else{
        $('#new-tag-div').hide();
        $('#new-tag-button').val('Nuevo tag');
        $('#new-tag').removeClass('error');
        $('label.error').remove();
      }
    });
    /*$('#create_tag').validate({
        rules: {
          'new_tag' : {required: true},
        },
        messages: {
          'new_tag' : {required: "Campo requerido"},
        }
      });*/
    $('#create_tag').submit(function() {
      if($('#new_tag').val()==''){
        $('#new_tag_invalid').text("Debe ingresar un nombre").show().fadeOut(2500);
        return false;
      } else {
        return true;
      }
    });
    $('#create_comment').submit(function() {
      if($('#new_post').val()==''){
        $('#new_post_invalid').text("No puede ingresar un texto vacío").show().fadeOut(2500);
        return false;
      } else {
        return true;
      }
    });
    $('.post .btn.btn-danger.btn-small.right').hide();
    $('.post').hover(
      function () {
        $(this).find('.btn.btn-danger.btn-small.right').show();
      },
      function () {
        $(this).find('.btn.btn-danger.btn-small.right').hide();
      }
    );
    $('.complaint-container .complaint-btn a').hide();
    $('.complaint-container').hover(
      function () {
        $(this).find('.complaint-btn a').show();
      },
      function () {
        $(this).find('.complaint-btn a').hide();
      }
    );
    //$('.btn.btn-danger.btn-small.right')
}
//--------------------------------------
// FILE UPLOAD
//--------------------------------------
function file_upload_init() {
  if($('#upload').length > 0){
      $("#upload").bind("click", function() {
        /* Generating unique id
        */
        var rand = Math.random().toString().split(".")[1];
        var input = '<div><input type="file" name="files['+rand+']"/><a class="btn btn-small btn-danger" id='+rand+'>Cancelar</a></div>'
        $(this).before(input );
        $('#'+rand).click(function(){
          $(this).closest('div').slideUp('slow', function(){
            $(this).remove();});
          }); 
      });

      /* 
        Pushing the first input to the DOM
      */
      $("#upload").trigger("click");
  }
}
//--------------------------------------
// AUTOCOMPLETE
//--------------------------------------

function Close(element){
  $(element).parent().remove();
}

function autocomplete_init(){

    $('#send_message').click(function() {
      if($('#personas-tags-div').children().length==0)
      {
        alert("Debes ingresar receptores");
        return false;
      }else{
        return true;
      }
    });
    

    $('#message_to').autocomplete({
      source: "/ajax/msg_all",
      select: function( event, ui ){
        if($("#"+ui.item.name).length==0)
        {
          $('#personas-tags-div').append("<div class='token'>"+ui.item.label+"<input type='hidden' name='participantes[]' id="+ui.item.name+" value='"+ui.item.name+"'><a href='#' class='close closeButtonSmall' onclick='Close(this)'>x</a></div>");
          $(this).val('');
          return false;
        }
      }
    });
    $("#new_msg").validate({
      rules: {
        'asunto' : {required: true},
        'mensaje' : {required: true}
      },
      messages: {
          'asunto' : {required: "Asunto requerido"},
          'mensaje':{required:"Mensaje requerido"}
    }});
    $('#post_user_name').autocomplete({source: "/ajax/users"});
    $('#post_company_name').autocomplete({source: "/ajax/companies"});
    $('#complaint_company_name').autocomplete({
      source: "/ajax/companies_all"
    });
    $('#tag').autocomplete({source: "/ajax/tags_all"});
}
//--------------------------------------
// INICIACIÓN
//--------------------------------------
$(

  function(){
    registro_init();
    complaint_init();
    file_upload_init();
    autocomplete_init();
    $('#admin_menu').menu();
    $('#cause-tabs').tabs();
  }
  
);


