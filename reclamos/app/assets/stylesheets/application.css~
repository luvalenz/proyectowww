/*
 * This is a manifest file that'll be compiled into application.css, which will include all the files
 * listed below.
 *
 * Any CSS and SCSS file within this directory, lib/assets/stylesheets, vendor/assets/stylesheets,
 * or vendor/assets/stylesheets of plugins, if any, can be referenced here using a relative path.
 *
 * You're free to add application-wide styles to this file and they'll appear at the top of the
 * compiled file, but it's generally better to create a new file per style scope.
 *
 *= require_self
 *= require_tree .
 */
 
/*----------------------------------------------------------------------------------------------------
  RESET AL CSS
----------------------------------------------------------------------------------------------------*/

html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite,  del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td {
	margin:0px; 
	padding:0px;
	border:0px; 
	border-collapse:separate; 
	border-spacing:0px;
	font-weight: inherit;
	font-style: inherit;
	font-size: 100%;
	text-decoration:none;
}
html, body {
	height: 100%;
	width: 100%;
  	margin: 0 0 1px; /*-- Forces ScrollBar --*/
}
body {
	font-family:  Helvetica, Arial, sans-serif;
	font-size: 12px;
	line-height: 17px;
	float: left;
}
h1,h2,h3,h4,h5,h6{
	font-family: 'Ubuntu Condensed', Helvetica, Arial, sans-serif;
	margin-bottom: 8px;
	font-weight: normal;
	color: #333;
}
h1{
	font-size:40px;
	line-height: 42px;
}
h2{
	font-size: 32px;
	line-height: 30px;
}
h3{
	font-size:24px;
	line-height: 27px;
}
h4{
	font-size:20px;
	line-height: 23px;	
}
h5{
	font-size:18px;
	line-height: 20px;
}
h6{
	font-size:14px;
	line-height: 15px;
}
ul, ol	{
	display: block;
	padding: 0px;
}
p {
	margin-bottom: 15px;
	line-height: 22px;
	color: black;
}
p:last-child {margin-bottom: 5px;}
em {font-style: italic;}
strong, b , .bold{font-weight: bold;}
a { 
	-webkit-transition: all 0.3s ease;
	-moz-transition: all 0.3s ease;
	-o-transition: all 0.3s ease;
	-ms-transition: all 0.3s ease;
	transition: all 0.3s ease;
}
img, iframe {max-width:100%;}

::-moz-selection{ color: #fff; background: #cccccc; }
::selection { color: #fff; background: #cccccc; }
 
/* * 
 {
  padding:0px;
  margin:0px;
  font-family: 'Ubuntu','Arial', 'sans-serif';
 }
 
 
 h1{
  margin:2px;
 }
 
 a:visited{
  text-decoration:none;
 }
 a:link{
  text-decoration:none;
 }
 */
 
/*----------------------------------------------------------------------------------------------------
	Body
----------------------------------------------------------------------------------------------------*/ 
  body{
    width:100%;
    background-color:whiteSmoke;
  }
 
/*----------------------------------------------------------------------------------------------------
	Header
----------------------------------------------------------------------------------------------------*/

.fixposition {
	position: fixed;
	float: left;
	height: auto;
	width: 100%;
	z-index: 1000;
}

#header-wrapper {
  background-color: #0F1B41;
	height: 100px;
	width: auto;
	box-shadow: 0px 1px 5px rgba(0,0,0,.5);
	-moz-box-shadow: 0px 1px 5px rgba(0,0,0,.5);
	-webkit-box-shadow: 0px 1px 5px rgba(0,0,0,.5);
	-khtml-box-shadow: 0px 1px 5px rgba(0,0,0,.5);
}
#header-content {
	width: 940px;
	height: 100px;
	margin-top: 0;
	margin-right: auto;
	margin-bottom: 0;
	margin-left: auto;
	padding-right: 10px;
	padding-left: 10px;
}

/*----------------------------------------------------------------------------------------------------
	Header Nav
----------------------------------------------------------------------------------------------------*/

.menu{
	float: left;
}

.menu ul{
	margin: 0;
	padding: 0;
	list-style-type: none;
}

.menu ul li{
	display: inline;
	float: left;
}

.menu ul li a{
	display: block;
	text-decoration: none;
	height: 50px;
	padding-top: 50px;
	padding-right: 15px;
	padding-left: 15px;
	font-family: 'Actor', sans-serif;
	font-size:14px;
	color:white;
}

* html .menu ul li a{ /*IE6 hack*/
	display: inline-block;
}
.menu ul li a:link, .menu ul li a:visited{
}
.menu ul li a.selected{ /*CSS class that's dynamically added to the currently active menu items' LI A element*/
	color: white;
	background-image: url(../images/bg_menuHover.png);
	background-repeat: repeat-x;
}
.menu ul li a:hover {
	background-color:#F2931D;
}

 /* ============================
     UserBar
    ============================ */ 
 
 fieldset#userBar{
  width:100%;
  background-color:#1B1B1B;
  height:25px;
  border:0px;
  border-bottom: 1px solid red;
  color:#88C0FF;
 }
 
 fieldset#userBar a{
  color:#88C0FF;
  font-weight:bold;
 }
 
  fieldset#userBar ul{
  float:right;
  width:500px;
 }
 
 fieldset#userBar li{
  height:25px;
  list-style: none;
  list-style-type: none;
  float: left;
  width:auto;
  margin-left:2px;
  margin-right:2px;
  padding-left:2px;
  padding-right:2px;
  text-align:center;
 }
 
fieldset#userBar li:hover{
  background-color:#373737;
 }
 
 /* ============================
     login
    ============================ */
 
 fieldset#loginBar{
  float:right;
  margin-top: 30px;
  padding-top:7px;
  width:400px;
  height:auto;
  border:0px;
  color:#88C0FF;
 }
 
 fieldset#loginBar a{
  color:#4D90FE;
  text-decoration:underline;
  font-weight:bold;
 }
 
 span.registro{
  font-size:12px;
 }
 
 fieldset#loginBar .container{
  float:right;
  width:500px;
  margin-right:50px;
 }
 
 .loginText{
  border-radius: 4px;
  width: 100px;
  height: 19px;
  color: white;
  font-weight:bold;
  background-color: #2B485C;
  border: solid 1px #0B5E92;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  -khtml-border-radius: 4px;
  -webkit-appearance: none;
}
 
 fieldset#loginBar div.field{
  display:inline;
 }
  
 #loginButton{
  border: 1px solid black;
  border-radius: 5px;
  color: black;
  text-shadow: 0 1px rgba(0, 0, 0, 0.3);
  background-color: #F5FFFF;
  background:-webkit-gradient (radial, 508 1000, 0, 121 -257, 465, from(#000000), to(#F5FFFF));
  font-weight:bold;
  height:20px;
  line-height: 100%;
  padding:2px;
 }

 #loginButton:hover{
  box-shadow: -1px 1px 20px #4D90FE;
  background-color:#2B485C;
  color:white;
  cursor:pointer;
 }

 input.loginText:focus {
  border:1px solid #4D90FE;
  -webkit-box-shadow:0 1px 2px rgba(0, 0, 0, 0.3) inset;
  -moz-box-shadow:0 1px 2px rgba(0, 0, 0, 0.3) inset;
  box-shadow:0 1px 2px rgba(0, 0, 0, 0.3) inset;
  outline: none;
 }
 
 /* ============================
     Navegación
    ============================ */ 
 
 nav#tabs{
  margin:0px auto;
  width:100%;
  height:25px;
  border-bottom: 1px solid red;
  background: #a90329; /* Old browsers */
  background: -moz-linear-gradient(top,  #a90329 0%, #8f0222 44%, #6d0019 100%); /* FF3.6+ */
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#a90329), color-stop(44%,#8f0222), color-stop(100%,#6d0019)); /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top,  #a90329 0%,#8f0222 44%,#6d0019 100%); /* Chrome10+,Safari5.1+ */
  background: -o-linear-gradient(top,  #a90329 0%,#8f0222 44%,#6d0019 100%); /* Opera 11.10+ */
  background: -ms-linear-gradient(top,  #a90329 0%,#8f0222 44%,#6d0019 100%); /* IE10+ */
  background: linear-gradient(to bottom,  #a90329 0%,#8f0222 44%,#6d0019 100%); /* W3C */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a90329',    endColorstr='#6d0019',GradientType=0 ); /* IE6-9 */
 }
 
 nav#tabs{
  width:100%;
  margin:0px auto;
  height:35px;
 }
 
 nav#tabs ul{
  width:960px;
  height:auto;
  padding-top:5px;
  margin:0px auto;
 }
 
 nav#tabs li{
  float:left;
  list-style: none;
 }
 
 nav#tabs li a{
  text-decoration:none;
  color:#FFF;
  margin-left:15px;
 }

 /* ============================
     Registro
    ============================ */


 /* ============================
     Administración
    ============================ */
    
#admin_menu{
  float:left;
  width:200px;
  height:150px;
}

#admin_content{
  float:left;
  margin-left:10px;
  padding:5px;
  padding-left:13px;
  background-color:white;
    -moz-border-radius: 10px;
  -webkit-border-radius: 10px;
  border-radius: 10px;
}
    
ul#admin_panel{
  width:100%;
  margin:0px auto;
}
ul#admin_panel a{
  color:blue;
  text-decoration:none;
}

ul#admin_panel li.section{
  list-style: none;
  float:left;
  width:70%;
}

ul#admin_panel li.section_first{
  list-style: none;
  float:left;
  width:30%;
}

nav#admin_tools ul{
  list-style:none;
}

 /* ============================
     Reclamos
    ============================ */

/*
    Lista
*/

ul#complaints_list{
  width:80%;
  height:auto;
  margin:0px auto;
}

ul#complaints_list li{
  display: block;
  list-style-type: none;
  margin: 2px;
  padding: 5px;
}

dl.complaints{
  width:580px;
  margin:0px auto;
  background-color:#C8C8C8;
  border-width: 1px;
  -moz-border-radius: 15px;
  -webkit-border-radius: 15px;
  border-radius: 15px;
  padding:5px;
  padding-left:20px;
  padding-right:20px;
  -moz-box-shadow: 1px 0px 11px #000000;
  -webkit-box-shadow: 1px 0px 11px #000000;
  box-shadow: 1px 0px 11px #000000;
  border:1px solid #3E5795;
}

dl.complaints:hover{
  transition-property: border;
  transition-duration: 2s;
  border:1px solid #FFBC00;
}

dl.complaints a{
  color:#a90329;
}

dl.complaints span.complaint_name a{
  color:#3E5795;
  font-weight:bold;
}

.complaint{
  width:700px;
  margin:0px auto;
  color:#333;
}

.form-container{
  border-bottom: dotted #BBB;
  border-width: 4px 0px; 
  padding-bottom: 10px;
  margin:0px auto;
  margin-bottom:20px;
  padding:5px;
  padding-left:50px;
  width:650px;
  border: 1px solid #BBB;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  -khtml-border-radius: 4px;
}

.complaint-container{
  background-color:white;
  border-bottom: dotted #BBB;
  border-width: 4px 0px; 
  padding-bottom: 10px;
  margin-bottom:20px;
  
  padding:5px;
  border: 1px solid #BBB;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  -khtml-border-radius: 4px;
}
h2.complaint-title{
  padding: 5px 5px;
  color: #333;
  font-weight: normal;
  border-top: dotted #BBB;
  border-width: 1px 0px;
}
h2.complaint-attach{
  font-size:17px;
  color: #333;
  margin-left: 10px;
}
h1.complaint-subtitle{
  padding: 10px 0px;
  /*border-top: 4px dotted #BBB;*/
  border-bottom: 1px none #BBB;
  padding-top: 5px;
  padding-bottom: 10px;
  margin-top: 10px;
  margin-bottom: 5px;
  font-weight:bold;
}
.complaint-subtitle a{
  color: #3E5795;
}
.complaint-attach-list{
  list-style:none;
  margin-left:20px;
}
.complaint-info{
  border-bottom: 1px solid #BBB;
  padding-bottom:10px;
  padding-top:5px;
  height:35px;
  font-weight:12px;
}
.cause-info{
  padding-bottom:10px;
  padding-top:5px;
  height:35px;
  font-weight:12px;
}
.complaint-tags-div{
  background-color:white;
  padding:5px;
  border: 1px solid #BBB;
  margin-bottom: 10px;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  -khtml-border-radius: 4px;
  font-size: 13px;
}
h2.comentario{
  margin-bottom:10px;
}
.complaint-author{
  margin-left:7px;
  color: #333;
  font-size:12px;
  padding-top:10px;
}
.complaint-tags{
  margin-left:20px;
  margin-top:0px;
}

.sombra-linda{
border-radius: 5px 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; 
box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.1); -webkit-box-shadow: 5px 5px rgba(0, 0, 0, 0.1); -moz-box-shadow: 5px 5px rgba(0, 0, 0, 0.1);
}

.posts_list{
  background-color:white;
  padding:4px;
  color:black;
}
.tag-single{
  display:inline;
  width:auto;
  font-size:13px;
}
#new-tag-div{
  float:left;
  margin-left:30px;
}
#new-tag-button{
  float:left;
}
#new-tag-buttons{
  height:50px;
  margin-top:8px;
}
.complaint-description{
  padding:10px;
  padding-top:5px;
  font-size:13px;
}

.new_comment{
  margin:0px auto;
  text-align:center;
}

.post-info{
  border-bottom:1px dotted #BBB;
  color: #444;
  height:32px;
}
.post-body{
  padding:5px;
  margin-bottom:10px;
}

.post:hover{
  -webkit-transition:background-color 0.5s linear;  
  background:#DDD;  
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  -khtml-border-radius: 4px;
}
.new_post{
  margin:0px auto;
  text-align:center;
  margin-top:20px;
}
#new_post_invalid{
  color:red;
  font-weight:bold;
}
#new_tag_invalid{
  font-size:14px;
  font-weight:bold;
  color:red;
}
.complaint-cause{
  margin-left:20px;
}
/*
    Lista show/id
*/

ul#complaints_show_list{
  width:80%;
  height:auto;
}

ul#complaints_show_list li{
  display: block;
  list-style-type: none;
  margin: 2px;
  padding: 5px;
  float:left
}

ul#complaints_show_list li.information{
  width:10%;
}
ul#complaints_show_list li.description{
  width:80%;
}

span.complaint-name{
  color:#3E5795;
  font-weight:bold;
  padding:2px;
  font-size:20px;
  float:left;
}

.complaint-btn{
  text-align:right;
  margin:0px;
  height:30px;
}

span.creator_name a{
  color:#a90329;
  font-weight:bold;
}
span.creator_name:visited{
  color:#a90329;
  font-weight:bold;
}

span.red{
  color:#a90329;
  font-weight:bold;
}

/*=================================================================================*/
 
 .container{
  width:960px;
  margin:0px auto;
 }
 
 #session{
  float: right;
  padding-right:20px;
  width:auto;
 }
 
 #session div{
  display:inline;
 }
 #session input[type="text"],input[type="password"]{
  display:inline;
  width:100px;
 }
 
#session input:focus
{ 
  /*background-color:#e9f6fd;*/
  -webkit-box-shadow: 1px 1px 22px #e9f6fd;
  -moz-box-shadow:    1px 1px 22px #e9f6fd;
  box-shadow:         1px 1px 22px #e9f6fd;
}
#session input:hover
{
  -webkit-box-shadow: 1px 1px 22px #e9f6fd;
  -moz-box-shadow:    1px 1px 22px #e9f6fd;
  box-shadow:         1px 1px 22px #e9f6fd;
} 

span.button{
  width:auto;
  background-color: #000;
  color:#FFF;
  padding:5px;
  margin-left:2px;
  -webkit-border-radius: 30px;
  -moz-border-radius: 30px;
  border-radius: 30px;
}
span.button a{
  color:#FFF;
  text-decoration:none;
}

header span.box{
  width:auto;
  background-color: #000;
  color:#FFF;
  padding:5px;
  margin-left:2px;
  -webkit-border-radius: 30px;
  -moz-border-radius: 30px;
  border-radius: 30px;
}

header span.box:hover{
  -webkit-box-shadow: 1px 1px 22px yellow;
  -moz-box-shadow:    1px 1px 22px yellow;
  box-shadow:         1px 1px 22px yellow;
}

header span.box a{
  color:#FFF;
  text-decoration:none;
}

#wrapper{
  margin:0px auto;
  width:960px;
  height:auto;
  margin-top:30px;
}


div.clear{
  clear:both;
} 

.new_user input[type="text"],.new_user input[type="password"]{
  width:286px;
}

/*.new_user input:focus{
  -webkit-box-shadow: 1px 1px 22px #258dc8;
  -moz-box-shadow:    1px 1px 22px #258dc8;
  box-shadow:         1px 1px 22px #258dc8;
}
.new_user input:hover{
  -webkit-box-shadow: 1px 1px 22px #258dc8;
  -moz-box-shadow:    1px 1px 22px #258dc8;
  box-shadow:         1px 1px 22px #258dc8;
}*/

.btn {
  color: white;
  border-color: #c5c5c5;
  border-color: rgba(0, 0, 0, 0.15) rgba(0, 0, 0, 0.15) rgba(0, 0, 0, 0.25);
  margin: 3px;
  display:inline-block;
}

.btn-large {
  padding: 11px 19px;
  font-size: 17.5px;
  -webkit-border-radius: 6px;
     -moz-border-radius: 6px;
          border-radius: 6px;
}

.btn-small {
  padding: 2px 10px;
  font-size: 11.9px;
  -webkit-border-radius: 3px;
     -moz-border-radius: 3px;
          border-radius: 3px;
}

.btn-tag{
  border-color: #c5c5c5;
  border-color: rgba(0, 0, 0, 0.15) rgba(0, 0, 0, 0.15) rgba(0, 0, 0, 0.25);
  margin: 0px;
  display:inline-block;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.4);
}
.btn-tag-small{
  padding: 2px 10px;
  font-size: 11.9px;
  border:none 3px #000000;
  -moz-border-radius-topleft: 3px;
  -moz-border-radius-topright:0px;
  -moz-border-radius-bottomleft:3px;
  -moz-border-radius-bottomright:0px;
  -webkit-border-top-left-radius:3px;
  -webkit-border-top-right-radius:0px;
  -webkit-border-bottom-left-radius:3px;
  -webkit-border-bottom-right-radius:0px;
  border-top-left-radius:3px;
  border-top-right-radius:0px;
  border-bottom-left-radius:3px;
  border-bottom-right-radius:0px;
}
.btn-tag-remove{
  padding: 2px 10px;
  font-size: 11.9px;
  border:3px none #000000;
  -moz-border-radius-topleft: 0px;
  -moz-border-radius-topright:3px;
  -moz-border-radius-bottomleft:0px;
  -moz-border-radius-bottomright:3px;
  -webkit-border-top-left-radius:0px;
  -webkit-border-top-right-radius:3px;
  -webkit-border-bottom-left-radius:0px;
  -webkit-border-bottom-right-radius:3px;
  border-top-left-radius:0px;
  border-top-right-radius:3px;
  border-bottom-left-radius:0px;
  border-bottom-right-radius:3px;
}

.btn-success {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #5bb75b;
  *background-color: #51a351;
  background-image: -moz-linear-gradient(top, #62c462, #51a351);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#62c462), to(#51a351));
  background-image: -webkit-linear-gradient(top, #62c462, #51a351);
  background-image: -o-linear-gradient(top, #62c462, #51a351);
  background-image: linear-gradient(to bottom, #62c462, #51a351);
  background-repeat: repeat-x;
  border-color: #51a351 #51a351 #387038;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff62c462', endColorstr='#ff51a351', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}

.btn-tag{
  font-size:13px;
  background-color: #C8C8C8;
  color: #FFF;
  margin:2px;
}
.btn-tag a{
  color: #000;
}
.btn-tag:hover{
  background-color: orange;
}
.remove-tag:hover{
  color:red;
}
i.tags-i{
  font-size:13px;
  color:#3E5795;
  font-weight:bold;
}
i.tags-i a{
  font-size:13px;
  color:#3E5795;
  font-weight:normal;
  padding-right:3px;
  padding-left:2px;
}
i.tags-i a:hover{
  text-decoration:underline;
}


.btn-success:hover,
.btn-success:active,
.btn-success.active,
.btn-success.disabled,
.btn-success[disabled] {
  color: #ffffff;
  background-color: #51a351;
  *background-color: #499249;
  cursor: pointer;
}

.btn-success:active,
.btn-success.active {
  background-color: #408140 \9;
}


.btn-primary {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #006dcc;
  *background-color: #0044cc;
  background-image: -moz-linear-gradient(top, #0088cc, #0044cc);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#0088cc), to(#0044cc));
  background-image: -webkit-linear-gradient(top, #0088cc, #0044cc);
  background-image: -o-linear-gradient(top, #0088cc, #0044cc);
  background-image: linear-gradient(to bottom, #0088cc, #0044cc);
  background-repeat: repeat-x;
  border-color: #0044cc #0044cc #002a80;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0044cc', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}

.btn-primary:hover,
.btn-primary:active,
.btn-primary.active,
.btn-primary.disabled,
.btn-primary[disabled] {
  color: #ffffff;
  background-color: #0044cc;
  *background-color: #003bb3;
  -moz-box-shadow: 1px 0px 11px #000000;
-webkit-box-shadow: 1px 0px 11px black;
box-shadow: 1px 0px 11px black;
  cursor: pointer;
}

.btn-primary:active,
.btn-primary.active {
  background-color: #003399 \9;
}

.btn-danger {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #da4f49;
  *background-color: #bd362f;
  background-image: -moz-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b), to(#bd362f));
  background-image: -webkit-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -o-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: linear-gradient(to bottom, #ee5f5b, #bd362f);
  background-repeat: repeat-x;
  border-color: #bd362f #bd362f #802420;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b', endColorstr='#ffbd362f', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}

.btn-danger:hover,
.btn-danger:active,
.btn-danger.active,
.btn-danger.disabled,
.btn-danger[disabled] {
  color: #ffffff;
  background-color: #bd362f;
  *background-color: #a9302a;
  cursor: pointer;
}

.btn-danger:active,
.btn-danger.active {
  background-color: #942a25 \9;
}

#content{
  border: 5px solid #FFF;
  width:800px;
  height:auto;
  margin:0px auto;
  background-color:#CfCfCf;
  padding:30px;
  margin-top:110px;
  /*border:double 2px #000000;*/
	-moz-border-radius: 10px;
	-webkit-border-radius: 10px;
	-khtml-border-radius: 10px;
	border-radius: 10px;
	box-shadow: 0px 0px 4px rgba(0,0,0,.3);
	-moz-box-shadow: 0px 0px 4px rgba(0,0,0,.3);
	-webkit-box-shadow: 0px 0px 4px rgba(0,0,0,.3);
	-khtml-box-shadow: 0px 0px 4px rgba(0,0,0,.3);
}

/*Validation*/
label.error{
  color:red;
  font-weight:bold;
  background-color:white;
  border:solid 1px #000000;
  margin-left:5px;
  padding:2px;
  -moz-border-radius: 23px;
  -webkit-border-radius: 23px;
  border-radius: 23px;
-moz-box-shadow: 2px 3px 5px #000000;
-webkit-box-shadow: 2px 3px 5px #000000;
box-shadow: 2px 3px 5px #000000;
}

.right{
  float:right;
}
.left{
  float:left;
}

footer{
  width:100%;
  height:100px;
}

.token{
  background: #E2E6F0;
  border: 1px solid #9DACCC;
  -webkit-border-radius: 2px;
  color: #1C2A47;
  cursor: default;
  display: block;
  float: left;
  height: 20px;
  margin: 0 4px 4px 0;
  padding: 3px;
  position: relative;
  white-space: nowrap;
}

.close{
  margin: 2px 0 -2px 1px;
  outline: none;
  font-weight:bold;
}

.closeButtonSmall{
  height:11px;
  width:11px;
  padding:2px;
}

.close:hover{
  background:blue;
  color:white;
  cursor:pointer;
}

.no-border{
  border: none;
}

