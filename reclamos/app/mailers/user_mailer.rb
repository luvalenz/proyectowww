class UserMailer < ActionMailer::Base
  default :from => "reclamoscafe@gmail.com"
 
  def welcome_email(user)
    @user = user
    mail(:to => user.email, :subject => "Bienvenido a reclamos de cafe")
  end
  
  def send_password(user, code)
    @user = user
    @recovery_code = code
    
    mail(:to => user.email, :subject => "Recuperacion de contrasenia")
  end
  
  def send_invitation(inviting, invited, cause)
    @inviting = inviting
    @cause = cause
    mail(:to => invited.email, :subject => "Te han enviado una invitacion")
  end
  
  def accepted_invitation(inviting, invited, cause)
    @invited = invited
    @cause = cause
    mail(:to => inviting.email, :subject => "Han aceptado tu invitacion")
  end
  
  def rejected_invitation(inviting, invited, cause)
    @invited = invited
    @cause = cause
    mail(:to => inviting.email, :subject => "Han rechazado tu invitacion")
  end
  
end
