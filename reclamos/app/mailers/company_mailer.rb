class CompanyMailer < ActionMailer::Base
  default :from => "reclamoscafe@gmail.com"
 
  def welcome_email(company)
    @company = company
    mail(:to => company.email, :subject => "Bienvenido a reclamos de cafe")
  end
  
end
