require 'net/http'
require 'net/https'
require 'uri'
require 'json'

class Complaint < ActiveRecord::Base
  attr_accessible :frequent,:views, :name, :problem_date, :state, :description, :company_id, :cause_id,:company_name, :company
  
  belongs_to :creator, :class_name => "User"
  belongs_to :company
  belongs_to :cause
  
  has_many :documents,:dependent => :destroy
  has_many :posts,:dependent => :destroy
  
  has_and_belongs_to_many :tags
  
  validates :company_id, :presence => true
  
  def likes
  
    url = 'http://www.facebook.com/reclamos_cafe2/complaints/' + id.to_s
    json_s = get_facebook_stats(url)
    json_obj = JSON.parse(json_s)
    json_obj[0]["total_count"]
  
    
  end
  
  #http://stackoverflow.com/questions/7178590/facebook-fql-query-with-ruby
  def get_facebook_stats(url)

    params = {
        :query => 'SELECT total_count FROM link_stat WHERE url = "' + url + '"',
        :format => 'json'
    }

    http = http_get('api.facebook.com', '/method/fql.query', params)
    http

  end
  
  def http_get(domain,path,params)
    path = path + "?" + params.collect { |k,v| "#{k}=#{CGI::escape(v.to_s)}" }.join('&')
    request = Net::HTTP.get(domain, path)
  end
  
  def company_name
    company.name if company
  end

  def company_name=(name)
    unless name.blank?
      @company = Company.find_by_name(name)
      if @company then
        self.company = @company
      else
        @company = Company.new
        @company.update_attributes(:password => '123456',:state => '0',:name => name)
        if @company.save
          self.company = @company
        end
      end
    end
  end
end
