class Message < ActiveRecord::Base
  
  # Un mensaje tendra un solo creador: company, lawyer o user
  attr_accessible :company_id, :lawyer_id, :text, :user_id
  
  belongs_to :chat_room
  belongs_to :lawyer
  belongs_to :user
  belongs_to :company
  
  
  def add_current(user)
    if user.class.name == "User" then
      self.user = user
    elsif user.class.name == "Lawyer" then
      self.lawyer = user
    else
      self.company = user
    end
  end
  
end
