class DefenseRequest < ActiveRecord::Base
  attr_accessible :requested_cause_id, :requesting_lawyer_id

  belongs_to :requesting_lawyer, :class_name => 'Lawyer', :foreign_key => 'requesting_lawyer_id'
  belongs_to :requested_cause, :class_name => 'Cause', :foreign_key => 'requested_cause_id'
    
   
end
