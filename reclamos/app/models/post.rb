class Post < ActiveRecord::Base
  attr_accessible :complaint_id, :text, :user_id
  
  belongs_to :user
  belongs_to :complaint
  
end
