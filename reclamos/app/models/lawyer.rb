class Lawyer < ActiveRecord::Base
  attr_accessible :curriculum_path, :first_name, :last_name, :password, :username
  
  has_many :causes
  
  has_many :lawyer_invitations
  
  has_many :defense_requests, :foreign_key => 'requesting_lawyer_id'
  has_many :requested_causes, :through => :defense_requests, :class_name => 'Cause'
  
  
  attr_accessible :email, :first_name, :last_name, :password, :username,:accepted
  
  has_secure_password
  
  validates :email, :presence => true, :uniqueness => true
  validates :username, :presence => true, :uniqueness => true
  validates :password, :presence => true, :length => {:minimum => 6,:maximum => 20,:too_short => "Lo minimo permitido son %{count} letras",:too_long => "Lo maximo permitido son %{count} letras"}

  validate :user_and_company_username_exists

  def self.process_file(attrs)
    uploaded_file = attrs[:curriculum_path]
    if uploaded_file
      File.open(Rails.root.join('public/assets/uploads/curriculums', attrs[:curriculum_path].original_filename), 'wb') do |file|
        file.write(uploaded_file.read)
      end
      attrs[:curriculum_path] = uploaded_file.original_filename
    end
  end
  
  private
  def user_and_company_username_exists
    l = User.find_by_username(self.username)
    c = Company.find_by_username(self.username)
    if(l or c)
      #errors.add_to_base(:username,"El username ya existe")
      errors[:base] << "El username ya existe"
    end
  end

end
