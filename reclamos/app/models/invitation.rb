class Invitation < ActiveRecord::Base
  attr_accessible :state, :invited_id, :cause_id
    belongs_to :inviting, :class_name => 'User', :foreign_key => 'inviting_id'
    belongs_to :invited, :class_name => 'User', :foreign_key => 'invited_id'
    
    belongs_to :cause
    
  
end
