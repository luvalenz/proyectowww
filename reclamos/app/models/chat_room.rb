class ChatRoom < ActiveRecord::Base
  attr_accessible :name
  
  has_many :messages
  has_many :chats
  
end
