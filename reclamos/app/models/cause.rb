class Cause < ActiveRecord::Base
  attr_accessible :description, :name, :popularity, :state, :company_id
  
  has_many :users, :through => :supports
  has_many :supports
  
  belongs_to :company
  
  has_many :defense_requests, :foreign_key => 'requested_cause_id'
  has_many :requesting_lawyers, :through => :defense_requests, :class_name => 'Lawyer'
  
  belongs_to :lawyer
  
  has_many :complaints
  
  has_many :invitations
  has_many :lawyer_invitations
  
  def creator
    owner_support = self.supports.where(:is_owner => true)[0]
    if (owner_support != nil)
      owner = User.find(owner_support.user_id)
    end
  end
  
   
end
