class Company < ActiveRecord::Base
  attr_accessible :email, :name, :password, :state, :username,:id
  
  has_secure_password
  
  has_many :causes
  
  has_many :complaints
  validates :name, :presence => {true => "Debe ingresar un Nombre"}, :uniqueness => true
  
  validate :user_and_lawyer_username_exists
  
  private
  def user_and_lawyer_username_exists
    l = User.find_by_username(self.username)
    c = Lawyer.find_by_username(self.username)
    if(l or c)
      #errors.add_to_base(:username,"El username ya existe")
      errors[:base] << "El username ya existe"
    end
  end
  
end
