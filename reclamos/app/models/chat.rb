class Chat < ActiveRecord::Base
  attr_accessible :company_id, :lawyer_id, :user_id,:chat_room_id,:news
  
  belongs_to :user
  belongs_to :lawyer
  belongs_to :company
  belongs_to :chat_room
  
end
