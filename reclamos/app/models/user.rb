class User < ActiveRecord::Base
  has_many :created_complaints, :foreign_key => "creator_id", :class_name => "Complaint", :dependent => :destroy
  
  has_many :causes, :through => :supports
  has_many :supports
  
  has_many :sent_invitations, :class_name => "Invitation", :foreign_key => "inviting_id",:dependent => :destroy
  has_many :received_invitations, :class_name => "Invitation", :foreign_key => "invited_id",:dependent => :destroy
  
  has_many :posts,:dependent => :destroy
  
  has_many :recovery_codes
  
  attr_accessible :email, :first_name, :last_name, :password, :username, :admin, :access_token
  
  has_secure_password
  
  validates :email, :presence => true, :uniqueness => true
  validates :username, :presence => {true => "Debe ingresar un Username"}, :uniqueness => true
  validates :password, :presence => true, :length => {:minimum => 6,:maximum => 20,:too_short => "Lo minimo permitido son %{count} letras",:too_long => "Lo maximo permitido son %{count} letras"}
  
  validate :lawyer_and_company_username_exists
  
  private
  def lawyer_and_company_username_exists
    l = Lawyer.find_by_username(self.username)
    c = Company.find_by_username(self.username)
    if(l or c)
      #errors.add_to_base(:username,"El username ya existe")
      errors[:base] << "El username ya existe"
    end
  end
  
end
