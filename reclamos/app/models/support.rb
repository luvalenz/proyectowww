class Support < ActiveRecord::Base
  attr_accessible :is_owner
  
  belongs_to :user
  belongs_to :cause
  
end
